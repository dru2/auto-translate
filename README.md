# Auto Translator

Translate a list of phrases into a list of languages

## Usage
1. Fork / Clone this repo
2. Install requirements: `pip install -r requirements.txt`  
3. Add the phrases you want to translate to `phrases.txt`
4. Optional: modify the list of languages in `langagues.py`
5. Run from the commmand line `python translate.py` or `python3 translate.py`
6. Take a break while your device does all the heavy lifting
7. Open up the `translations/` sub-directory for your translations when the script has run 

#### Notes
* To avoid installing requirements globally, use a virtual environment: 
  * First use `python -m venv venv` to create a virtual environment
  * Every time you start a new session: `source venv/bin/activate` 
  * Install requirements and run the script from within your safe space 
* Occassionally, the script my error out due to phrases ending up translating to the same thing. If this happens, try adding the phrase with a space character at the end, or restarting the script from where you left off. Take care to not re-run and add the same phrases. 
* After running the script on new phrases for the **ZEPHYRx Provider Dashboard**, please add to the end of "all-phrases.txt" so we maintain a record of all translated phrases 

--- 

## Languages
Any language supported by Google Translate 📚

--- 

## Tech 
* Python 3.9.1 🐍
* Selenium 
* Chromedriver-py
* A little help from [Google Translate](https://translate.google.com)
